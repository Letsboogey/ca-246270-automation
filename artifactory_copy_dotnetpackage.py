import sys
import os
import requests
import getopt

API = "https://repo.citrite.net/artifactory/api"
SRC_REPO = "xc-local-build"
DST_REPO = "xc-local-release"

def arti_req(s, method, full=False):
    """Artifactory request"""
    if full:
        url = s
    else:
        url = '{0}/{1}'.format(API, s)
    r = requests.request(method, url)
    if r.ok:
        return r.json()
    r.raise_for_status()

def arti_post(s, full=False):
    """Artifactory POST request"""
    return arti_req(s, 'POST', full)

def arti_copy(src, dst):
    """
    ArtifactoryRESTAPI - CopyItem
    Desciption: copy an artifact or folder to specified destination
    """
    result = arti_post('copy/{0}/?to={1}/'.format(src, dst))
    #print result to console output
    print("\n".join(x["message"] for x in result["messages"]))

def main():
    #get path parameters from Jenkins
    LOCATION = os.environ.get("LOCATION")
    BRANCH = os.environ.get("BRANCH")
    PACKAGE_NUMBER = os.environ.get("PACKAGE_NUMBER")

    src ='{0}/{1}/{2}/{3}'.format(SRC_REPO,LOCATION,BRANCH,PACKAGE_NUMBER)
    dst = '{0}/{1}'.format(DST_REPO, LOCATION)

    arti_copy(src, dst)
        
if __name__ == "__main__":
    main()

